# prudentialcicd

Case study project

Here i am considering the AWS EC 2 instance:
we can use 2 ways: 

1. Using shell script - shell scrpit is good bcz we can install directly using script

2. Using ansible - its configuration management tool and needs to be configured,  before using ansible scripts

1. Using shell script

We will be using Amazon AMI running centos 7. we will follow following steps to install the Jenkins CI server on Amazon AMI.

1.1 Launch the Amazon EC2 instance from an Amazon Machine Image (AMI).

1.2 Create a security group and allow allow SSH (port 22) and 8080 to access your Jenkins dashboard from local machine.
    To improve security we can can define pubile ip range to allow access to your ec2 server.

1.3 Connect to your jenkins system using ssh with PEM keys

1.4 Here we are assuming installtion sripts are on git repository, so we will install git using 
    sudo yum -y update
    sudo yum install git

1.5 clone the scripts using git clone command
    git clone https://gitlab.com/ydeopura/prudentialcicd.git

1.6 move inside directory
    cd prudentialcicd

1.7 exectue the script
    sudo su
    chmod 755 script.sh
    sh script.sh

1.8 It’s not a best practice to assign jenkins user sudo permission. But, in some cases you may require to run script which needs sudo permission to access particular directory. \
    In that cases,we can assign the jenkins user sudo permissions as following.
    sudo vim /etc/sudoers
    jenkins ALL=(ALL) NOPASSWD: ALL

1.9 We can access Jenkins server using the public DNS on port 8080.
    http://{ec2-public-dns}:8080  and then configure the jenkins using initial password.
    
1.10 once jenkins is installed please install the required plugins.

------------------------------------------------------------------------

2 Using ansible
We need Ansible installed on host system before using this.

2.1 create playbook site.yml

2.2 create file in location roles/jenkins/tasks/main.yml

2.3 run playbook ansible-playbook -i inventory/hosts site.yml

2.4 monitor the logs jenkins will be installed.

2.5 check the machine public ip allocated to instance and then open it in browser and then configure the jenkins using initial password.

2.6 We can access Jenkins server using the public DNS on port 8080.

http://{ec2-public-dns}:8080 and then configure the jenkins using initial password.

2.7 once jenkins is installed please install the required plugins.



-----------------------------------------------------------------------------

Pipeline script for CI to fetch the codebase from https://github.com/ahfarmer/emoji-search.git and package it for deployment
launch the application in dev environment for  every 6 hours.

1.1 created jenkins pipeline script (jenkinspipeline ) that will fetch the code and deploy the application.

please refer jenkinspipeline file for the same

1.2 launch the application in dev environment for every new commit.

We have configured jenkins build triggers for the same.
triggers are configured in jenkins with gitlab plugin and in gitlab proivded the jenkins details with job name.


------------------------------------------------------------------------

Deploy using docker :

1.1 we need to create a dockerfile.please find dockerfile in repo.

1.2 run docker build -t emoji-search . to create docker image

1.3 run docker images to check docker images.

1.4 docker run -p 80:3000 {image-id} to deploy image in docker.

Security Consideration:

1.1 In AWS we can only allow ports in inbound that are needed.also we can map the ip wit ports that are allowed to access the URL so that only some ip have access to URL.








